'use server'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export async function createTarefa(conteudo) {
    const tarefa = await prisma.tarefa.create({
        data: {
            Conteudo: conteudo,
        }
    })
    console.log(tarefa)
}


export async function findAllTarefas() {
    const tarefas = await prisma.tarefa.findMany({
        orderBy: [
            { Finalizada: 'asc' },
            { DataCriacao: 'desc' }
        ]
    })
    return tarefas
}

export async function updateStatus(tarefaId, status) {
    const tarefa = await prisma.tarefa.update({
        data: {
            Finalizada: status
        },
        where: {
            Id: tarefaId
        }
    })
    console.log(tarefa)
}


export async function updateConteudo(tarefaId, conteudo) {
    const tarefa = await prisma.tarefa.update({
        data: {
            Conteudo: conteudo
        },
        where: {
            Id: tarefaId
        }
    })
    console.log(tarefa)
}

export async function deleteTarefa(tarefaId) {
    const tarefa = await prisma.tarefa.delete({
        where: {
            Id: tarefaId
        }
    })
    console.log(`Tarefa do id ${tarefa.Id} deletada!`)
}