"use client";
import FormTask from "./components/FormTask";
import { findAllTarefas, createTarefa, deleteTarefa } from "./services/TarefaService";
import { useState, useEffect } from "react";
import ListaTarefas from "./components/ListaTarefas";

export default function Home() {
  const [tarefas, setTarefas] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchTarefas = async () => {
      try {
        const tarefasData = await findAllTarefas();
        setTarefas(tarefasData);
        setLoading(false);
      } catch (error) {
        console.error("Erro ao obter as tarefas:", error);
      }
    };

    fetchTarefas();
  }, []);

  const handleCreateTask = async (tarefa) => {
    setLoading(true);
    await createTarefa(tarefa);
    const updatedTarefas = await findAllTarefas();
    setTarefas(updatedTarefas);
    setLoading(false);
  };

  const handleDeleteTask = async (idTarefa) => {
    setLoading(true);
    await deleteTarefa(idTarefa);
    const updatedTarefas = await findAllTarefas();
    setTarefas(updatedTarefas);
    setLoading(false);
  };

  console.log(tarefas);

  
  return (
    <div className="container my-5">
      <div className="text-center">
        <h1>To-Do List</h1>
        <FormTask onCreateTask={handleCreateTask} />
      </div>

      {loading ? (
        <div className="row my-4">
          <div className="col d-flex justify-content-center">
            <strong role="status">Carregando...</strong>
            <div className="spinner-border ms-3" aria-hidden="true"></div>
          </div>
        </div>
      ) : (
        <ListaTarefas tarefas={tarefas} setTarefas={setTarefas} onDeleteTask={handleDeleteTask} />
      )}
    </div>
  );
}
