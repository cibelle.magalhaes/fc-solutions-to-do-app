import { useState } from "react";
import { updateStatus, updateConteudo } from "../services/TarefaService";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { PencilFill, Trash3Fill } from "react-bootstrap-icons";

export default function ListaTarefas({ tarefas, setTarefas, onDeleteTask }) {
  const [showEditar, setShowEditar] = useState(Array(tarefas.length).fill(false));
  const [tarefaSelecionada, setTarefaSelecionada] = useState(null);
  const [novoConteudoTarefa, setNovoConteudoTarefa] = useState("");

  const handleCloseEditar = async () => {
    setShowEditar(showEditar.map(() => false));

    
    if (tarefaSelecionada && novoConteudoTarefa !== "") {
      const updatedTarefas = tarefas.map(tarefa =>
        tarefa === tarefaSelecionada ? { ...tarefa, Conteudo: novoConteudoTarefa } : tarefa
      );
      setTarefas(updatedTarefas);
      try {
        await updateConteudo(tarefaSelecionada.Id, novoConteudoTarefa);
      } catch (error) {
        console.error("Erro ao atualizar o conteúdo da tarefa:", error);
      }
    }
    setTarefaSelecionada(null);
    setNovoConteudoTarefa("");
  };

  const handleShowEditar = (index) => {
    setShowEditar(showEditar.map((value, i) => i === index ? true : value));
    setTarefaSelecionada(tarefas[index]);
    setNovoConteudoTarefa(tarefas[index].Conteudo);
  };

  const handleCheckboxChange = async (index) => {
    const isFinalizada = tarefas[index].Finalizada;

    try {
      await updateStatus(tarefas[index].Id, !isFinalizada);
    } catch (error) {
      console.error("Erro ao atualizar o status da tarefa:", error);
    }

    const updatedTarefas = [...tarefas];
    updatedTarefas[index].Finalizada = !isFinalizada;
    setTarefas(updatedTarefas);
  };

  const handleConteudoTarefaChange = (event) => {
    setNovoConteudoTarefa(event.target.value);
  };


  const handleDeleteTarefa = async (idTarefa) => {
    try {
      onDeleteTask(idTarefa)
    
    } catch (error) {
      console.error("Erro ao deletar a tarefa:", error);
    }
  };


  return (
    <div className="row justify-content-center my-4">
      <div className="col-5">
        <ul className="list-group">
          {tarefas.map((tarefa, index) => (
            <li className="list-group-item" key={index}>
              <div className="row">
                <div className="col-1">
                  <input
                    className="form-check-input me-1"
                    type="checkbox"
                    value=""
                    id={`checkbox-${index}`}
                    checked={tarefa.Finalizada}
                    onChange={() => handleCheckboxChange(index)}
                  />
                </div>

                <div className="col-8">
                  <label
                    className={
                      tarefa.Finalizada
                        ? "form-check-label text-decoration-line-through"
                        : "form-check-label"
                    }
                    htmlFor={`checkbox-${index}`}
                  >
                    {tarefa.Conteudo}
                  </label>
                </div>

                <div className="col-1 gx-2">
                  <Button variant="primary" onClick={() => handleShowEditar(index)}>
                    <PencilFill />
                  </Button>
                </div>

                <div className="col-1">
                  <Button variant="danger" onClick={() => handleDeleteTarefa(tarefa.Id)}>
                    <Trash3Fill />
                  </Button>
                </div>
              </div>
              <Modal show={showEditar[index]} onHide={handleCloseEditar}>
                <Modal.Header closeButton>
                  <Modal.Title>Editar Tarefa</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <input
                    type="text"
                    className="form-control"
                    value={novoConteudoTarefa}
                    onChange={handleConteudoTarefaChange}
                  />
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="success" onClick={handleCloseEditar}>
                    Salvar
                  </Button>
                </Modal.Footer>
              </Modal>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
