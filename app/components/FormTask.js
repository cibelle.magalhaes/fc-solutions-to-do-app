'use client'
import React, { useState } from 'react';
export default function FormTask({ onCreateTask }){

    const [tarefa, setTarefa] = useState('');

    const handleInputChange = (event) => {
        setTarefa(event.target.value);
    };
    
    const handleAddTask = async () => {
        onCreateTask(tarefa);
        setTarefa('')
      
    };

    const handleKeyUp = (event) => {
        if (event.key === 'Enter') {
            handleAddTask();
        }
    };

    return(
        <div className="row justify-content-center my-4">
        <div className="col-5">
            <div className="input-group mb-3">
                <input 
                    type="text" 
                    className="form-control" 
                    placeholder="Digite uma tarefa" 
                    aria-label="Digite uma tarefa" 
                    aria-describedby="button-addon2"
                    value={tarefa}
                    onChange={handleInputChange}
                    onKeyUp={handleKeyUp}
                />
                <button 
                    className="btn btn-outline-secondary" 
                    type="button" 
                    id="button-addon2"
                    onClick={handleAddTask}
                >
                    Adicionar
                </button>
            </div>
        </div>
    </div>
    )
}