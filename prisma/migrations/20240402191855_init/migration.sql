-- CreateTable
CREATE TABLE "Tarefa" (
    "Id" SERIAL NOT NULL,
    "Conteudo" TEXT NOT NULL,
    "DataCriacao" TIMESTAMP(3) NOT NULL,
    "DataConclusao" TIMESTAMP(3),
    "Finalizada" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Tarefa_pkey" PRIMARY KEY ("Id")
);
