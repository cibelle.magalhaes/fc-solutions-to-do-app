This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Antes de começar

Primeiro instale as dependencias do projeto com:

```bash
npm install
```

Depois rode as migrations do prisma com:

```bash
npx prisma migrate dev
```

## Rodando o projeto

Bom, agora você pode rodar o projeto com:

```bash
npm run dev
```

Abra [http://localhost:3000](http://localhost:3000) com seu navegador para ver o resultado.

